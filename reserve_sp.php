<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php'); ?>
<?php
  $c = Page::getCurrentPage();
  $iframe = h($c->getAttribute('iframe'));
  $iframeH = h($c->getAttribute('iframe_height'));
?>
    <div id="main">
      <div class="container">
        <article>
          <h1>予約</h1>
          <?php
            $a = new Area('Main');
            $a->setAreaGridMaximumColumns(2);
            $a->display($c);
          ?>
          <div class="boxSet nomargin iframeWrapper">
            <?php if($iframe) : ?>
              <iframe src="<?php echo nl2br(h($iframe)); ?>" name="a" frameborder="0" scrolling="auto" vspace="0" width="100%" height="<?php echo nl2br(h($iframeH)); ?>" align="center">
                このブラウザはインラインフレームに対応しておりません。インラインフレーム対応のブラウザでご覧ください。
              </iframe>
            <?php endif; ?>
          </div>
          <?php
            $a = new Area('Bottom');
            $a->setAreaGridMaximumColumns(2);
            $a->display($c);
          ?>
        </article>
      </div>
    </div>
<?php $this->inc('elements/footer.php'); ?>
 
