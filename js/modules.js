var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

var _isPC = !(_ua.Mobile || _ua.Tablet);

$(function(){
  // img.retinaへの処理
  var $retinaCheck = window.devicePixelRatio;
	if(_isPC && $retinaCheck >= 2) {
		$('img.retina').each( function() {
      if($(this).attr('data-src')){
        var $retinaimg = $(this).attr('data-src').replace(/\.(?=(?:png|jpg|jpeg)$)/i, '@2x.');
  			$(this).attr('srcset', $retinaimg + " 2x");
      } else {
        var $retinaimg = $(this).attr('src').replace(/\.(?=(?:png|jpg|jpeg)$)/i, '@2x.');
  			$(this).attr('srcset', $retinaimg + " 2x");
      }
		});
	}
});

$(function(){
  // ページトップ
	$('#pageTop a').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 400);
		return false;
	});
	$(window).scroll(function(){
		var $scrollNum = $(window).scrollTop();
    var $scrollHeight = $(document).height();
    var $scrollPosition = $(window).height() + $scrollNum;
    var $footHeight = $('footer').innerHeight();
		if($scrollNum >= 20){
			$('#pageTop').addClass('on');
      if ($scrollHeight - $scrollPosition <= $footHeight){
        $('#pageTop').css({'position':'absolute', 'bottom': $footHeight + 100 + 'px'});
      } else {
        $('#pageTop').css({'position':'fixed', 'bottom': '100px'});
      }
		} else {
			$('#pageTop').removeClass('on');
		}
	});
});

$(function(){
  // URL処理
	var $url = $.url();
	var $dire;
	$dire = $url.segment(1);
  var $path = $url.attr('path');
  var $file = $url.attr('file');
  var $directory = $url.attr('directory');
  if($dire === ''){
    $dire = 'index';
  }
  if($file === 'index.html'){
    $path = $directory;
  }
  /*
	$('#gnav').find('a').each(function() {
    var $key  = $(this).attr('class');
		if ($key === $dire) {
			$(this).addClass('active');
		}
	});
  */
});

$(function(){
  // SP用Languageボタン
  var $headerSpLang = $('#headerSpLang');
  var $headerSpLangChild = $('#headerSpLangChild');
  // SP用メニュー
  var $headerSpMenu = $('#headerSpMenu');
  var $headerSpMenuChild = $('#headerSpMenuChild');
  var $headerSpMenuClose = $('#headerSpMenuClose');
  // PC用Languageボタン
  var $headerPcLang = $('#headerPcLang');
  var $headerPcLangChild = $('#headerPcLangChild');
  // PC用プラン一覧
  var $headerPlanListBtn = $('#headerPlanListBtn');
  var $headerPlanList = $('#headerPlanList');
  // PC用店舗検索
  var $headerShopListBtn = $('#headerShopListBtn');
  var $headerShopList = $('#headerShopList');

  var $spBtns = [$headerSpLang, $headerSpMenu, $headerSpMenuClose];
  var $spLists = [$headerSpLangChild, $headerSpMenuChild];

  var $pcBtns = [$headerPcLang, $headerPlanListBtn, $headerShopListBtn];
  var $pcLists = [$headerPcLangChild, $headerPlanList, $headerShopList];

  var $spReset = function(){
    $.each($spBtns, function() {
      $(this).removeClass('active');
    });
    $.each($spLists, function() {
      $(this).hide();
    });
  }
  var $pcReset = function(){
    $.each($pcBtns, function() {
      $(this).removeClass('active');
    });
    $.each($pcLists, function() {
      $(this).hide();
    });
  }

  // SP用Languageボタン
  $headerSpLang.on('click',function(){
    if($(this).hasClass('active')){
      $spReset();
    } else {
      $spReset();
      $(this).addClass('active');
      $headerSpLangChild.show();
    }
  });

  // SP用メニュー
  $headerSpMenu.on('click',function(){
    if($(this).hasClass('active')){
      $spReset();
    } else {
      $spReset();
      $(this).addClass('active');
      $headerSpMenuChild.show();
    }
  });
  $headerSpMenuClose.on('click',function(){
    $spReset();
  });

  // PC用Languageボタン
  $headerPcLang.on('click',function(){
    if($(this).hasClass('active')){
      $pcReset();
    } else {
      $pcReset();
      $(this).addClass('active');
      $headerPcLangChild.show();
    }
  });

  // PC用プラン一覧ボタン
  $headerShopListBtn.on('click',function(){
    if($(this).hasClass('active')){
      $pcReset();
    } else {
      $pcReset();
      $(this).addClass('active');
      $headerShopList.show();
    }
  });

  // PC用店舗検索ボタン
  $headerPlanListBtn.on('click',function(){
    if($(this).hasClass('active')){
      $pcReset();
    } else {
      $pcReset();
      $(this).addClass('active');
      $headerPlanList.show();
    }
  });

});

// halfImgSetのスライダー
var $halfImgSetTimer = false;
$(window).on('load resize', function(){
  var $halfImgSet = $('.halfImgSet');
  if($halfImgSet.size()){
    $halfImgSet.each(function(){
      if($(window).width() < 640 && !$(this).hasClass('slick-initialized')) {
        if ($halfImgSetTimer !== false) {
  				clearTimeout($halfImgSetTimer);
  			}
        $halfImgSetTimer = setTimeout(function(){
          $halfImgSet.slick({
            dots: true,
            speed: 100,
             adaptiveHeight: true
          });
        }, 200);
      } else {
        if($(this).hasClass('slick-initialized')) {
          $(this).slick('unslick');
        }
      }
    });
  }
});

// oneThirdSetのtile
$(window).on('load resize', function(){
  var $oneThirdSet = $('.oneThirdSet');
  var $num = 3;
  if($oneThirdSet.size()){
    $oneThirdSet.each(function(){
      if($(this).hasClass('blogTop')) {
        $num = 2;
      }
      if(!$(window).width() < 640) {
        $(this).find('figure').tile($num);
        $(this).find('.boxSet:first-of-type').tile($num);
      } else {
        $(this).find('figure').css('height','auto');
        $(this).find('.boxSet:first-of-type').css('height','auto');
      }
    });
  }
});
