var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());


(function ($) {

    $(function () {
        if (!supportsInputAttribute('placeholder')) {
            $('[placeholder]').each(function () {
                var $this = $(this),
                    $form = $this.closest('form'),
                    placeholderText = $this.attr('placeholder'),
                    placeholderColor = 'GrayText',
                    defaultColor = $this.css('color');
                $this.bind({
                    focus: function () {
                        if ($this.val() === placeholderText) {
                            $this.val('').css('color', defaultColor);
                        }
                    },
                    blur: function () {
                        if ($this.val() === '') {
                            $this.val(placeholderText).css('color', placeholderColor);
                        } else if ($this.val() === placeholderText) {
                            $this.css('color', placeholderColor);
                        }
                    }
                });
                $this.trigger('blur');
                $form.submit(function () {
                    if ($this.val() === placeholderText) {
                        $this.val('');
                    }
                });
            });
        }
    });

    // detect support for input attirbute
    function supportsInputAttribute (attr) {
        var input = document.createElement('input');
        return attr in input;
    }

})(jQuery);


/*

	skOuterClick - A simple event-binder-plugin to handle click events of outside elements.
	Copyright (c) 2014 SUKOBUTO All rights reserved.
	Licensed under the MIT license.

*/

(function($){

	$.fn.skOuterClick = function(method) {
		var methods = {
			init : function (handler) {
				var inners = new Array();
				if (arguments.length > 1) for (i = 1; i < arguments.length; i++) {
					inners.push(arguments[i]);
				}
				return this.each(function() {
					var self = $(this);
					var _this = this;
					var isInner = false;
					// Bind click event to suppress
					function onInnerClick(e){
						isInner = true;
					};
					self.click(onInnerClick);
					for (var i = 0; i < inners.length; i++) {
						inners[i].click(onInnerClick);
					}
					// Bind click elsewhere
					$(document).click(function(e){
						if (!isInner) handler.call(_this, e);
						else isInner = false;
					});
				});
			}
		};
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'function') {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method "' + method + '" does not exist in skOuterClick plugin!');
		}
	};
})(jQuery);


/*
 CSS Browser Selector js v0.5.3 (July 2, 2013)

 -- original --
 Rafael Lima (http://rafael.adm.br)
 http://rafael.adm.br/css_browser_selector
 License: http://choosealicense.com/licenses/mit/
 Contributors: http://rafael.adm.br/css_browser_selector#contributors
 -- /original --

 Fork project: http://code.google.com/p/css-browser-selector/
 Song Hyo-Jin (shj at xenosi.de)
 */
function css_browser_selector(e){var i=e.toLowerCase(),r=function(e){return i.indexOf(e)>-1},t="gecko",o="webkit",a="safari",n="chrome",s="opera",d="mobile",c=0,l=window.devicePixelRatio?(window.devicePixelRatio+"").replace(".","_"):"1",p=[!/opera|webtv/.test(i)&&/msie\s(\d+)/.test(i)&&(c=1*RegExp.$1)?"ie ie"+c+(6==c||7==c?" ie67 ie678 ie6789":8==c?" ie678 ie6789":9==c?" ie6789 ie9m":c>9?" ie9m":""):/edge\/(\d+)\.(\d+)/.test(i)&&(c=[RegExp.$1,RegExp.$2])?"ie ie"+c[0]+" ie"+c[0]+"_"+c[1]+" ie9m edge":/trident\/\d+.*?;\s*rv:(\d+)\.(\d+)\)/.test(i)&&(c=[RegExp.$1,RegExp.$2])?"ie ie"+c[0]+" ie"+c[0]+"_"+c[1]+" ie9m":/firefox\/(\d+)\.(\d+)/.test(i)&&(re=RegExp)?t+" ff ff"+re.$1+" ff"+re.$1+"_"+re.$2:r("gecko/")?t:r(s)?s+(/version\/(\d+)/.test(i)?" "+s+RegExp.$1:/opera(\s|\/)(\d+)/.test(i)?" "+s+RegExp.$2:""):r("konqueror")?"konqueror":r("blackberry")?d+" blackberry":r(n)||r("crios")?o+" "+n:r("iron")?o+" iron":!r("cpu os")&&r("applewebkit/")?o+" "+a:r("mozilla/")?t:"",r("android")?d+" android":"",r("tablet")?"tablet":"",r("j2me")?d+" j2me":r("ipad; u; cpu os")?d+" chrome android tablet":r("ipad;u;cpu os")?d+" chromedef android tablet":r("iphone")?d+" ios iphone":r("ipod")?d+" ios ipod":r("ipad")?d+" ios ipad tablet":r("mac")?"mac":r("darwin")?"mac":r("webtv")?"webtv":r("win")?"win"+(r("windows nt 6.0")?" vista":""):r("freebsd")?"freebsd":r("x11")||r("linux")?"linux":"","1"!=l?" retina ratio"+l:"","js portrait"].join(" ");return window.jQuery&&!window.jQuery.browser&&(window.jQuery.browser=c?{msie:1,version:c}:{}),p}!function(e,i){var r=css_browser_selector(navigator.userAgent),t=e.documentElement;t.className+=" "+r;var o=r.replace(/^\s*|\s*$/g,"").split(/ +/);i.CSSBS=1;for(var a=0;a<o.length;a++)i["CSSBS_"+o[a]]=1;var n=function(i){return e.documentElement[i]||e.body[i]};i.jQuery&&!function(e){function r(){if(0==m){try{var e=n("clientWidth"),i=n("clientHeight");if(e>i?u.removeClass(a).addClass(s):u.removeClass(s).addClass(a),e==b)return;b=e}catch(r){}m=setTimeout(o,100)}}function o(){try{u.removeClass(w),u.addClass(360>=b?d:640>=b?c:768>=b?l:1024>=b?p:"pc")}catch(e){}m=0}var a="portrait",s="landscape",d="smartnarrow",c="smartwide",l="tabletnarrow",p="tabletwide",w=d+" "+c+" "+l+" "+p+" pc",u=e(t),m=0,b=0;i.CSSBS_ie?setInterval(r,1e3):e(i).on("resize orientationchange",r).trigger("resize"),e(i).load(r)}(i.jQuery)}(document,window);


(function(e){e.fn.tile=function(t){var n,r,i,s,o,u,a=document.body.style,f=["height"],l=this.length-1;if(!t)t=this.length;u=a.removeProperty?a.removeProperty:a.removeAttribute;return this.each(function(){u.apply(this.style,f)}).each(function(u){s=u%t;if(s==0)n=[];r=n[s]=e(this);o=r.css("box-sizing")=="border-box"?r.outerHeight():r.innerHeight();if(s==0||o>i)i=o;if(u==l||s==t-1){e.each(n,function(){this.css("height",i)})}})}})(jQuery)


/**
 * jQuery Unveil
 * A very lightweight jQuery plugin to lazy load images
 * http://luis-almeida.github.com/unveil
 *
 * Licensed under the MIT license.
 * Copyright 2013 Luís Almeida
 * https://github.com/luis-almeida
 */

;(function($) {

  $.fn.unveil = function(threshold, callback) {

    var $w = $(window),
        th = threshold || 0,
        retina = window.devicePixelRatio > 1,
        attrib = retina? "data-src-retina" : "data-src",
        images = this,
        loaded;

    this.one("unveil", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        this.setAttribute("src", source);
        if (typeof callback === "function") callback.call(this);
      }
    });

    function unveil() {
      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.is(":hidden")) return;

        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();

        return eb >= wt - th && et <= wb + th;
      });

      loaded = inview.trigger("unveil");
      images = images.not(loaded);
    }

    $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);

    unveil();

    return this;

  };

})(window.jQuery || window.Zepto);



// JQuery URL Parser plugin - https://github.com/allmarkedup/jQuery-URL-Parser
// Written by Mark Perkins, mark@allmarkedup.com
// License: http://unlicense.org/ (i.e. do what you want with it!)

;(function($, undefined) {

    var tag2attr = {
        a       : 'href',
        img     : 'src',
        form    : 'action',
        base    : 'href',
        script  : 'src',
        iframe  : 'src',
        link    : 'href'
    },

	key = ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","fragment"], // keys available to query

	aliases = { "anchor" : "fragment" }, // aliases for backwards compatability

	parser = {
		strict  : /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,  //less intuitive, more accurate to the specs
		loose   :  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // more intuitive, fails on relative paths and deviates from specs
	},

	querystring_parser = /(?:^|&|;)([^&=;]*)=?([^&;]*)/g, // supports both ampersand and semicolon-delimted query string key/value pairs

	fragment_parser = /(?:^|&|;)([^&=;]*)=?([^&;]*)/g; // supports both ampersand and semicolon-delimted fragment key/value pairs

	function parseUri( url, strictMode )
	{
		var str = decodeURI( url ),
		    res   = parser[ strictMode || false ? "strict" : "loose" ].exec( str ),
		    uri = { attr : {}, param : {}, seg : {} },
		    i   = 14;

		while ( i-- )
		{
			uri.attr[ key[i] ] = res[i] || "";
		}

		// build query and fragment parameters

		uri.param['query'] = {};
		uri.param['fragment'] = {};

		uri.attr['query'].replace( querystring_parser, function ( $0, $1, $2 ){
			if ($1)
			{
				uri.param['query'][$1] = $2;
			}
		});

		uri.attr['fragment'].replace( fragment_parser, function ( $0, $1, $2 ){
			if ($1)
			{
				uri.param['fragment'][$1] = $2;
			}
		});

		// split path and fragement into segments

        uri.seg['path'] = uri.attr.path.replace(/^\/+|\/+$/g,'').split('/');

        uri.seg['fragment'] = uri.attr.fragment.replace(/^\/+|\/+$/g,'').split('/');

        // compile a 'base' domain attribute

        uri.attr['base'] = uri.attr.host ? uri.attr.protocol+"://"+uri.attr.host + (uri.attr.port ? ":"+uri.attr.port : '') : '';

		return uri;
	};

	function getAttrName( elm )
	{
		var tn = elm.tagName;
		if ( tn !== undefined ) return tag2attr[tn.toLowerCase()];
		return tn;
	}

	$.fn.url = function( strictMode )
	{
	    var url = '';

	    if ( this.length )
	    {
	        url = $(this).attr( getAttrName(this[0]) ) || '';
	    }

        return $.url( url, strictMode );
	};

	$.url = function( url, strictMode )
	{
	    if ( arguments.length === 1 && url === true )
        {
            strictMode = true;
            url = undefined;
        }

        strictMode = strictMode || false;
        url = url || window.location.toString();

        return {

            data : parseUri(url, strictMode),

            // get various attributes from the URI
            attr : function( attr )
            {
                attr = aliases[attr] || attr;
                return attr !== undefined ? this.data.attr[attr] : this.data.attr;
            },

            // return query string parameters
            param : function( param )
            {
                return param !== undefined ? this.data.param.query[param] : this.data.param.query;
            },

            // return fragment parameters
            fparam : function( param )
            {
                return param !== undefined ? this.data.param.fragment[param] : this.data.param.fragment;
            },

            // return path segments
            segment : function( seg )
            {
                if ( seg === undefined )
                {
                    return this.data.seg.path;
                }
                else
                {
                    seg = seg < 0 ? this.data.seg.path.length + seg : seg - 1; // negative segments count from the end
                    return this.data.seg.path[seg];
                }
            },

            // return fragment segments
            fsegment : function( seg )
            {
                if ( seg === undefined )
                {
                    return this.data.seg.fragment;
                }
                else
                {
                    seg = seg < 0 ? this.data.seg.fragment.length + seg : seg - 1; // negative segments count from the end
                    return this.data.seg.fragment[seg];
                }
            }

        };

	};

})(jQuery);
