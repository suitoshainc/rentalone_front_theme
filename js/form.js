(function($) {
$(function(){
	//accordion
	var accordion_content = $('#accordion').find('.panel-collapse');
	$('#accordion').find('.box-header').css({'cursor':'pointer'}).on('click',function(){
		var target = $(this).next('.panel-collapse');
		if(target.css('display') == 'block'){
			target.slideUp('200');
		}else{
			accordion_content.slideUp('200');
			target.slideDown('200');
		}
	});
	//template change
	$('#template_type').change(function(){
		var target = $(this).val();
		var has_target = _.find(MAIL_REPLY_FORMAT , function(item){
			return item.name == target;
		});

		if(has_target){
			$('#message').val(has_target.body);
		}
	});
	/* select
	----------------------- */
	_.each($('.base_form').find('select'),function(item){
		var $select = $(item);
		$select.wrap('<div class="select_wrapper" />').after('<input type="text" value="" class="dummy_select" />');

		var $dummy = $select.next('.dummy_select');

		var default_val = $select.find("option:selected").text();
		$dummy.val(default_val);

		$select.change(function(){
			var val = $(this).find("option:selected").text();
			$dummy.val(val);
		});
	});

	/* radio
	----------------------- */
	var toggle_radio_active = function(obj){
		obj.parent('.form_parts').removeClass('checked');
		_.each(obj,function(o){
			if($(o).prop('checked') === true){
				$(o).parent('.form_parts').addClass('checked');
			}else{
				$(o).parent('.form_parts').removeClass('checked');
			}
		});
	};
	$(window).on('load',function(){
		toggle_radio_active($('.radio_group'));
	});

	_.each($('.radio_group'),function(item){
		var $radio = $(item);
		$radio.change(function(){
			toggle_radio_active($('.radio_group'));
		});
	});
	/* erroe msg
	----------------------- */
	_.each($('.error_msg'),function(item){
		$(item).parent('.form_content').addClass('has_error');
	});
});
})(jQuery);