<?php
namespace Application\Theme\Rentalone;

use Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;
use Concrete\Core\Page\Theme\Theme;

class PageTheme extends \Concrete\Core\Page\Theme\Theme implements ThemeProviderInterface {

  public function registerAssets()
  {
      $this->requireAsset('javascript', 'jquery');
      $this->requireAsset('javascript', 'underscore');
  }

  public function getThemeAreaClasses()
  {
    return array(
      '2列セット' => array('blue,point_list')
    );
  }
public function getThemeBlockClasses()
{
    return array(
        'image' => array(
            'side_yellow'
        ),
        'content' => array(
	        'yellow',
	        'rental_plan',
	        'blog_header_bar'
        ),
        'page_attribute_display' => array(
	        'date_center',
        ),
        'page_title' => array(
	        'white_back',
	        'gray_bar',
	        'shop_name_text',
        ),
        'tags' => array(
	        'tag_with_title',
        ),
        'pc_sp_images' => array(
	        'side_yellow',
        ),
    );
}

  public function getThemeEditorClasses()
  {
    return array(
      array('title' => t('赤文字'), 'menuClass' => '', 'spanClass' => 'red', 'wrap' => 'span'),
      array('title' => t('リード文'), 'menuClass' => '', 'spanClass' => 'lead', 'wrap' => 'p'),
      array('title' => t('三角矢印付きリンク'), 'menuClass' => '', 'spanClass' => 'link', 'wrap' => 'p'),
      array('title' => t('ボックス下部リンク'), 'menuClass' => '', 'spanClass' => 'more', 'wrap' => 'p'),
      array('title' => t('申し込みボタン（赤）'), 'menuClass' => '', 'spanClass' => 'btns red', 'wrap' => 'p')
    );
  }
  protected $pThemeGridFrameworkHandle = false;

  public function getThemeAreaLayoutPresets()
  {
    $presets = array(
      array(
        'handle' => 'two_column',
        'name' => '2列セット',
        'container' => '<div class="halfSet"></div>',
        'columns' => array(
        '<div class="unit"></div>',
        '<div class="unit"></div>'
        ),
      ),
      array(
        'handle' => 'three_column',
        'name' => '3列セット',
        'container' => '<div class="oneThirdSet"></div>',
        'columns' => array(
        '<div class="unit"></div>',
        '<div class="unit"></div>',
        '<div class="unit"></div>'
        ),
      ),
      array(
        'handle' => 'top_three_column',
        'name' => 'トップ用3列セット',
        'container' => '<div class="oneThirdSet topSet"></div>',
        'columns' => array(
        '<div class="unit"></div>',
        '<div class="unit"></div>',
        '<div class="unit"></div>'
        ),
      ),
      array(
        'handle' => 'four_column',
        'name' => '4列セット',
        'container' => '<div class="forthSet"></div>',
        'columns' => array(
        '<div class="unit"></div>',
        '<div class="unit"></div>',
        '<div class="unit"></div>',
        '<div class="unit"></div>'
        ),
      ),
      array(
        'handle' => 'two_image_column',
        'name' => '2画像列セット',
        'container' => '<div class="two_image_Set"></div>',
        'columns' => array(
        '<div class="unit"></div>',
        '<div class="unit"></div>'
        ),
      ),

      array(
        'handle' => 'shop_under_image',
        'name' => 'ショップ用2画像列セット',
        'container' => '<div class="shop_img_container"></div>',
        'columns' => array(
        '<div class="left_image"></div>',
        '<div class="right_image"></div>'
        ),
      ),
    );
    return $presets;
  }

}
