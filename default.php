<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php'); ?>
    <div id="main">
      <?php
        $a = new Area('VI');
        $a->display($c);
      ?>
      <div class="container">
        <article>
          <?php
            $a = new Area('Main');
            $a->setAreaGridMaximumColumns(2);
            $a->display($c);
          ?>
        </article>
      </div>
    </div>
<?php $this->inc('elements/footer.php'); ?>
