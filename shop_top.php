<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php'); ?>
<?php
	$c = Page::getCurrentPage();
	$nh = Loader::helper('navigation');
	$reserveBtnText = h($c->getAttribute('reserve_btn_text'));
	$reserveBtn = Page::getByID($c->getAttribute('reserve_btn_url'));
	$reserveBtnURL = $nh->getLinkToCollection($reserveBtn);
	$reserveBtnExternalURL = h($c->getAttribute('reserve_btn_external_url'));
	$mainImage = $c->getAttribute('main_image');
	if($mainImage){
		$mainImagePath = $mainImage->getVersion()->getRelativePath();
	}
	$access = h($c->getAttribute('access'));
	$address = h($c->getAttribute('address'));
	$businessHour = h($c->getAttribute('business_hour'));
	$tel = h($c->getAttribute('tel'));
	$closed = h($c->getAttribute('closed'));
	$notes = h($c->getAttribute('notes'));
?>
		<div id="main">
			<div class="container">
				<article>
					<h1 class="shop_name_text"><?php echo h($c->getCollectionName()); ?></h1>
					<?php
						$a = new Area('Main');
						$a->setAreaGridMaximumColumns(2);
						$a->display($c);
					?>
					<?php if($mainImage) : ?>
					 <!--  <div class="paragraphSet">
							<figure><img src="<?php echo $mainImagePath; ?>" alt="<?php echo h($c->getCollectionName()); ?>"></figure>
						</div> -->
					<?php endif; ?>
					<?php if($address or $businessHour or $tel) : ?>
						<div class="boxSet nomargin back_none">
							<table>
								<?php if($address) : ?>
									<tr>
										<th>住所</th>
										<td><?php echo nl2br(h($address)); ?></td>
									</tr>
								<?php endif; ?>
								<?php if($access) : ?>
									<tr>
										<th>アクセス</th>
										<td><?php echo nl2br(h($access)); ?></td>
									</tr>
								<?php endif; ?>
								<?php if($businessHour) : ?>
									<tr>
										<th>営業時間</th>
										<td><?php echo $businessHour; ?></td>
									</tr>
								<?php endif; ?>
								<?php if($tel) : ?>
									<tr>
										<th>電話番号</th>
										<td><?php echo $tel; ?></td>
									</tr>
								<?php endif; ?>
								<?php if($closed) : ?>
									<tr>
										<th>定休日</th>
										<td><?php echo $closed; ?></td>
									</tr>
								<?php endif; ?>
								<?php if($notes) : ?>
									<tr>
										<th>注意事項</th>
										<td><?php echo nl2br(h($notes)); ?></td>
									</tr>
								<?php endif; ?>
							</table>
						</div>
					<?php endif; ?>
					<?php
						$a = new Area('ShopMap');
						if ($c->isEditMode() || $a->getTotalBlocksInArea($c) > 0) {
							echo '<div class="map_wrapper">';
						}
						$a->display($c);
						if ($c->isEditMode() || $a->getTotalBlocksInArea($c) > 0) {
							echo '</div>';
						}

					?>
					<?php if($c->getAttribute('reserve_btn_url') != '0') : ?>
						<div style="margin-top:30px;margin-bottom:30px;">
							<p class="btns red"><a href="<?php echo $reserveBtnURL; ?>"><?php echo $reserveBtnText; ?></a></p>
						</div>
					<?php endif; ?>
					<?php if($reserveBtnExternalURL) : ?>
						<div class="paragraphSet">
							<p class="btns red"><a href="<?php echo $reserveBtnExternalURL; ?>" target="_blank"><?php echo $reserveBtnText; ?></a></p>
						</div>
					<?php endif; ?>
					<?php
						$a = new Area('RecentArticles');
						$a->display($c);
					?>
				</article>
			</div>
		</div>
<?php $this->inc('elements/footer.php'); ?>
