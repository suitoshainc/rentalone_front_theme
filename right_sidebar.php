<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
//リンク（要ナビゲーションヘルパー）
$nh = Core::make('helper/navigation');
$type = $c->getCollectionTypeHandle();
?>
<?php $this->inc('elements/header.php'); ?>
		<div id="main">
			<div class="container sidebar">
				<article>
					<?php
					if(in_array($type,array('tokyo_articles','blog_articles','kyoto_articles'))){
						$this->inc('elements/blog_header.php');
					};?>
					<?php
						if(in_array($type,array('tokyo_articles','blog_articles','kyoto_articles'))){
							echo '<div style="background:#fff;" class="blog_wrapper">';
						}
						$a = new Area('Main');
						$a->setAreaGridMaximumColumns(2);
						$a->display($c);
						if(in_array($type,array('tokyo_articles','blog_articles','kyoto_articles'))){
							echo '</div>';
						}
					?>

					<?php
						if(in_array($type,array('tokyo_articles','blog_articles','kyoto_articles'))):
					?>
					<div class="social_area">
						<ul>
						<li><div class="fb-share-button" data-href="<?php echo $nh->getLinkToCollection($c);?>" data-layout="button_count" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($nh->getLinkToCollection($c));?>&amp;src=sdkpreparse">シェア</a></div>
						</li>
						<li>
						<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $nh->getLinkToCollection($c);?>">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						</li>
						<li class="sp_">
						<div class="line-it-button" style="display: none;" data-type="share-a" data-lang="ja" ></div>
						<script src="//scdn.line-apps.com/n/line_it/thirdparty/loader.min.js" async="async" defer="defer" ></script>
						</li>
						</ul>
					</div>
						<?php
							$a = new GlobalArea('Blog Navigations');
							$a->display();
						?>
					<?php endif;?>
				</article>
				<aside id="lnav">
					<div class="sidebarBanners">
						<?php
							if($type == 'kyoto_articles'){
								$a = new GlobalArea('KyotoSidebarPost');
								$a->display();
							}elseif($type == 'tokyo_articles'){
								$a = new GlobalArea('TokyoSidebarPost');
								$a->display();
							}elseif($type == 'blog_articles'){
								$a = new GlobalArea('BloggSidebarPost');
								$a->display();
							}elseif($type == 'blog_top'){
								$a = new GlobalArea('BlogSidebarPost');
								$a->display();
							}
						?>
						<?php
							$a = new GlobalArea('SidebarBanners');
							$a->display($c);
						?>
					</div>
					<?php
						// $a = new Area('Sidebar');
						// $a->display($c);
					?>
				</aside>
			</div>
		</div>
<?php $this->inc('elements/footer.php'); ?>

