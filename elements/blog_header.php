<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$date = $c->getCollectionDatePublic();
$date = date('Y.m.d',strtotime($date));
?>
<div class="blog_header_bar">
	<h1>ブログ</h1>
</div>
<div class="date_center">
	<div class="entryDate"><?php echo $date;?></div>
</div>
<div class="white_back">
	<div class="titleArea">
		<div class=" with_underbar">
			<h2 class="page-title"><?php echo h($c->getCollectionName());?></h2>
		</div>
	</div>
</div>