<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
$c = Page::getCurrentPage();
$parent = Page::getByID($c->getCollectionParentID());
?>
<p id="pageTop"><a href="javascript:void(0);">このページの先頭へ</a></p>
<footer>
  <div class="top">
    <div class="container">
      <div class="sitemap">
        <nav class="sp">
          <?php
            $a = new GlobalArea('footerSp');
            $a->display();
          ?>
        </nav>
        <nav class="pc">
          <?php
            $a = new GlobalArea('footerPc');
            $a->display();
          ?>
        </nav>
        <ul class="sns">
          <?php
            $a = new GlobalArea('footerSns');
            $a->display();
          ?>
        </ul>
      </div>
      <div class="reserve">
        <?php
          $a = new GlobalArea('footerReserve');
          $a->display();
        ?>
      </div>
    </div>
  </div>
  <div class="bottom">
    <div class="container">
      <?php
        $a = new GlobalArea('copyright');
        $a->display();
      ?>
    </div>
  </div>
</footer>
</div>
<?php Loader::element('footer_required'); ?>
</body>
</html>
