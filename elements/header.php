<?php defined('C5_EXECUTE') or die(_("Access Denied."));
$nh = Core::make('helper/navigation');
$trail = $nh->getTrailToCollection($c);
$ancestors = array_reverse($trail);

$parentid = $c->getCollectionParentID();
$parent = Page::getByID($parentid);
$bodyclass = $parent->getCollectionHandle() .'-'.$c->getCollectionHandle();
?>
<!DOCTYPE html>
<html class="no-js" lang="<?php echo Localization::activeLanguage()?>">
<head>
	<?php Loader::element('header_required'); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="keyword" content="">
	<link href="<?php echo $view->getThemePath()?>/css/style.css" rel="stylesheet">
	<link href="<?php echo $view->getThemePath()?>/css/kimonolabo.css" rel="stylesheet">
	<script src="<?php echo $view->getThemePath()?>/js/plugins.js"></script>
	<script src="<?php echo $view->getThemePath()?>/js/jquery.cookie.js"></script>
	<script src="<?php echo $view->getThemePath()?>/js/slick.js"></script>
	<script src="<?php echo $view->getThemePath()?>/js/modules.js"></script>
	<script src="<?php echo $view->getThemePath()?>/js/ga.js"></script>
	<script src="<?php echo $view->getThemePath()?>/js/form.js"></script>
	<script src="https://use.fontawesome.com/39359d7993.js"></script>
	<?php if($c->isEditMode()):?>
		<style>
			header .container nav.sp {
				display: block;
			}
		</style>
	<?php endif;?>
	<?php
	$section = \Concrete\Core\Multilingual\Page\Section\Section::getCurrentSection();
	if ($section != null && $section->getLocale() == 'zh_CN') {
	?>
		<style>
			@import url(https://fonts.googleapis.com/earlyaccess/notosanssc.css);
			body{
				font-family: 'Noto Sans SC', sans-serif;
			}
		</style>
	<?php } ?>
	<?php
	// if($u->isLoggedIn()) {
	//   //ログイン状態で適用したい処理
	// }
	?>
</head>
<body <?php echo 'class="'.$bodyclass .'"';?>>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.8";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div id="wrapper" class="<?php echo $c->getPageWrapperClass()?><?php if(count($ancestors) > 1) echo ' lower';?>">
		<header class="main_header">
			<div class="container">
				<?php
				$a = new GlobalArea('headLogo');
				$a->display();
				?>
				<nav class="sp">
					<ul>
						<?php
						$a = new GlobalArea('headerSpLang');
						$a->display();
						?>
						<?php
						$a = new GlobalArea('headerSp');
						$a->display();
						?>
					</ul>
				</nav>
				<nav class="pc">
					<div class="util">
						<ul>
							<?php
							$a = new GlobalArea('headerPcLang');
							$a->display();
							?>
							<?php
							$a = new GlobalArea('headerPcSocial');
							$a->display();
							?>
						</ul>
					</div>
					<?php
					$a = new GlobalArea('headerPc');
					$a->display();
					?>
				</nav>
			</div>
		</header>
