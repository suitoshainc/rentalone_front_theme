<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php'); ?>
<div id="main">
	<div class="container">
		<article>
			<div class="paragraphSet">
				<?php Loader::element('system_errors', array('error' => $error)); ?>
				<p><?php print $innerContent; ?></p>

			</div>
		</article>
	</div>
</div>
<?php $this->inc('elements/footer.php'); ?>
